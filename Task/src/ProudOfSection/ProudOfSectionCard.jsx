import React from 'react';

export const ProudOfSectionCardUp = () => {
  return (
    <div className='flex flex-col w-[40%] h-[40vh] border-2  rounded-[25px] bg-white z-50'>
      <div className='flex justify-center items-center pt-2'>
        <div className='flex flex-1 justify-start space-x-2 ml-2'>
          <p className='border-2 rounded-[25px] bg-emerald-700 text-white px-1'>
            UI & UX
          </p>
          <p className='border-2 rounded-[25px] bg-pink-900 text-white px-1'>
            Development
          </p>
        </div>
        <p className='mr-5 text-black'>2022</p>
      </div>
      <div className='flex justify-center items-center p-4 pt-14'>
        <p className='text-4xl text-black text-left font-bold tracking-tight'>
          A company designed to translate and legalize important documents.
        </p>
      </div>
      <div className='flex justify-start items-start m-2 ml-5'>
        <button className='bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-lg flex flex-row'>
          See project
          <svg
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            stroke-width='1.5'
            stroke='currentColor'
            className='w-6 h-6 pl-2'
          >
            <path
              stroke-linecap='round'
              stroke-linejoin='round'
              d='M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25'
            />
          </svg>
        </button>
      </div>
    </div>
  );
};

export const ProudOfSectionCardRight = () => {
  return (
    <div className='flex flex-col w-[60%] h-[30vh] border-2  rounded-[25px] bg-white z-50'>
      <div className='flex justify-center items-center pt-2'>
        <div className='flex flex-1 justify-start space-x-2 ml-2'>
          <p className='border-2 rounded-[25px] bg-emerald-700 text-white px-1'>
            UI & UX
          </p>
          <p className='border-2 rounded-[25px] bg-pink-900 text-white px-1'>
            Development
          </p>
        </div>
        <p className='mr-5 text-black'>2022</p>
      </div>
      <div className='flex justify-center items-center p-4 pt-14'>
        <p className='text-4xl text-black text-left font-bold tracking-tight'>
          A platform for learning.
        </p>
      </div>
      <div className='flex justify-start items-start m-2 ml-5'>
        <button className='bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-lg flex flex-row'>
          See project
          <svg
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            stroke-width='1.5'
            stroke='currentColor'
            className='w-6 h-6 pl-2'
          >
            <path
              stroke-linecap='round'
              stroke-linejoin='round'
              d='M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25'
            />
          </svg>
        </button>
      </div>
    </div>
  );
};

export const ProudOfSectionCardDown = () => {
  return (
    <div
      id='cardComponent'
      className='flex flex-col w-[40%] h-[40vh] border-2  rounded-[25px] bg-white'
    >
      <div className='flex justify-center items-center pt-2'>
        <div className='flex flex-1 justify-start space-x-2 ml-2'>
          <p className='border-2 rounded-[25px] bg-orange-500 text-white px-1'>
            Video
          </p>
          <p className='border-2 rounded-[25px] bg-pink-600 text-white px-1'>
            Animation
          </p>
        </div>
        <p className='mr-5 text-black'>2022</p>
      </div>
      <div className='flex justify-center items-center p-4 pt-14'>
        <p className='text-4xl text-black text-left font-mono font-bold tracking-tight p-4'>
          Animations for Intellect App made
        </p>
      </div>
      <div className='flex justify-start items-start m-2 ml-5'>
        <button className='bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-lg flex flex-row'>
          See project
          <svg
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            stroke-width='1.5'
            stroke='currentColor'
            className='w-6 h-6 pl-2'
          >
            <path
              stroke-linecap='round'
              stroke-linejoin='round'
              d='M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25'
            />
          </svg>
        </button>
      </div>
    </div>
  );
};

export const ProudOfSectionBottomCardMobile = () => {
  return (
    <div className='flex flex-row justify-center items-center w-4/5 h-[7vh] border-2  rounded-[25px] bg-white z-50 px-4'>
      <p className='flex-1 text-2xl text-black text-left font-bold tracking-tight '>
        Prevodite.net
      </p>
      <svg
        xmlns='http://www.w3.org/2000/svg'
        fill='none'
        viewBox='0 0 24 24'
        stroke-width='1.5'
        stroke='currentColor'
        className='w-10 h-10 pl-2 text-green-600'
      >
        <path
          stroke-linecap='round'
          stroke-linejoin='round'
          d='M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25'
        />
      </svg>
    </div>
  );
};

export const ProudOfSectionTopCardMobile = () => {
  return (
    <div className='flex flex-1 justify-start space-x-2 ml-2'>
      <p className='border-2 rounded-[25px] bg-white text-green-600 px-4'>
        UI & UX
      </p>
      <p className='border-2 rounded-[25px]  bg-white text-green-600 px-4'>
        Development
      </p>
    </div>
  );
};

export const CaseStudyCard = () => {
  return (
    <div className='flex flex-1 justify-start space-x-2 ml-2'>
      <p className='border-2 rounded-lg text-green-600 px-4 border-black'>
        UI & UX
      </p>
      <p className='border-2 rounded-lg text-black px-4 border-black'>
        Development
      </p>
    </div>
  );
};
