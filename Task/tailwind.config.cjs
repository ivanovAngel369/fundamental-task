/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    screens: {
      'less': '100px',
      'sm': '425px',
      'md': '768px',
      'lg': '1024px',
    }
  },
  plugins: [],
}
