import React from 'react';
import One from '../../public/picktures/1.png';
import Two from '../../public/picktures/2.png';
import Three from '../../public/picktures/3.png';
import Four from '../../public/picktures/4.png';
import Five from '../../public/picktures/5.png';
import Six from '../../public/picktures/6.png';

const ServiceSection = () => {
  return (
    <div className="flex flex-col justify-center items-center bg-[url('../public/picktures/Lingu.png')] w-full  ">
      <div className='flex flex-col justify-center items-center '>
        <div className='flex flex-row justify-center items-center w-5/6 px-4'>
          <p className='text-6xl mb:text-4xl text-left font-bold mr-2 mt-4 w-full'>
            Services
          </p>
          <div className='md:flex-grow h-px bg-gray-400 mt-6'></div>
        </div>

        <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
          <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
            <img className='h-[15vh] md:h-[25vh]' src={One} alt='number One' />
            <p className='uppercase text-2xl md:text-3xl font-bold text-green-500 mt-4 w-full'>
              Website design
            </p>
          </div>
          <div className='flex justify-center items-center w-full md:w-1/2 '>
            <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>

        <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
          <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
            <img className='h-[15vh] md:h-[25vh]' src={Two} alt='number Two' />
            <p className='uppercase text-2xl md:text-3xl font-bold text-green-500 mt-4 w-full'>
              Website development
            </p>
          </div>
          <div className='flex justify-center items-center w-full md:w-1/2 '>
            <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
        <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
          <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
            <img
              className='h-[15vh] md:h-[25vh]'
              src={Three}
              alt='number Three'
            />
            <p className='uppercase text-2xl md:text-3xl font-bold text-green-500 mt-4 w-full'>
              Brand Identity
            </p>
          </div>
          <div className='flex justify-center items-center w-full md:w-1/2 '>
            <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
        <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
          <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
            <img
              className='h-[15vh] md:h-[25vh]'
              src={Four}
              alt='number Four'
            />
            <p className='uppercase text-2xl md:text-3xl font-bold text-green-500 mt-4 w-full'>
              Cms Development
            </p>
          </div>
          <div className='flex justify-center items-center full md:w-1/2 '>
            <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
        <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
          <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
            <img
              className='h-[15vh] md:h-[25vh]'
              src={Five}
              alt='number Five'
            />
            <p className='uppercase text-2xl md:text-3xl font-bold text-green-500 mt-4 w-full'>
              Software development
            </p>
          </div>
          <div className='flex justify-center items-center w-full md:w-1/2 '>
            <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
        <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
          <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
            <img className='h-[15vh] md:h-[25vh]' src={Six} alt='number Six' />
            <p className='uppercase text-2xl  md:text-3xl font-bold text-green-500 mt-4 w-full'>
              Animation
            </p>
          </div>
          <div className='flex justify-center items-center w-full md:w-1/2 '>
            <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ServiceSection;
