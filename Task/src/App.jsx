import CaseStudyPage from './CaseStudyPage/CaseStudyPage';
import Footer from './Footer/Footer';
import GetToKnowUsSection from './GetToKnowUsSection/GetToKnowUsSection';
import Header from './Header/Header';
import Home from './Home/Home';
import PopularOpinionSection from './PopularOpinion/PopularOpinionSection';
import ProudOfSection from './ProudOfSection/ProudOfSection';
import ServiceSection from './ServiceSection/ServiceSection';
import StartAProjectSection from './StartAProjectSection/StartAProjectSection';

function App() {
  return (
    <div className='App'>
      <div className="bg-[url('../public/picktures/gm1.png')] w-full min-h-screen">
        <Header />
        <Home />
        <ProudOfSection />
        <ServiceSection />
        <GetToKnowUsSection />
        <PopularOpinionSection />
        <StartAProjectSection />
        <Footer />
        <CaseStudyPage />
      </div>
    </div>
  );
}

export default App;
