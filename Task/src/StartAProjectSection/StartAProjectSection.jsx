import React from 'react';
import './StartAProjectSection.css';
const StartAProjectSection = () => {
  return (
    <div className="flex justify-center items-center w-full h-[75vh] bg-[url('../public/picktures/gm1.png')]">
      <div
        id='main'
        className='flex flex-col justify-center items-center w-full h-[75vh] space-y-5 md:space-y-6'
      >
        <p className='text-4xl md:text-8xl font-bold text-white text-center'>
          We would love to work with you
        </p>
        <p className='font-bold text-center text-white '>
          Fundamental Studio is a modern digital company, offering different
          kind of services at your disposal. we are your trustworthy partner.
        </p>
        <button className='bg-green-500 hover:bg-green-600 text-white font-bold px-14 py-6 md:py-4 md:px-4 rounded-lg flex flex-row md:mt-8 '>
          Start a project
          <svg
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            stroke-width='1.5'
            stroke='currentColor'
            className='w-6 h-6 pl-2 '
          >
            <path
              stroke-linecap='round'
              stroke-linejoin='round'
              d='M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25'
            />
          </svg>
        </button>
      </div>
    </div>
  );
};

export default StartAProjectSection;
