import React, { useState } from 'react';
import Logo from '../../public/picktures/logo.png';
import { AiOutlineFacebook } from 'react-icons/ai';
import { BsInstagram } from 'react-icons/bs';
import { AiFillLinkedin } from 'react-icons/ai';

const Header = () => {
  const [showNav, setShowNav] = useState(false);
  return (
    <div className='flex flex-col md:flex-row min-h-[5vh] bg-black/40 w-full p-2'>
      <div className='flex flex-1 flex-row justify-between items-center md:pl-40 px-2'>
        <img className='w-48 ' src={Logo} alt='Website logo' />
        <button onClick={() => setShowNav(!showNav)}>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            stroke-width='1.5'
            stroke='currentColor'
            class='w-7 h-7 text-white cursor-pointer md:hidden mb-1'
          >
            <path
              stroke-linecap='round'
              stroke-linejoin='round'
              d='M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5'
            />
          </svg>
        </button>
      </div>

      {showNav && (
        <div className='md:hidden flex flex-col justify-start less:backdrop-blur-lg absolute mt-14 w-[95%] h-[48vh] z-50 rounded-[14px] space-y-5'>
          <ul className='flex flex-col justify-start items-start w-full space-y-5'>
            <li className='font-bold text-white text-3xl cursor-pointer hover:text-gray-300 px-5'>
              Portfolio
            </li>
            <li className='font-bold text-white text-3xl cursor-pointer hover:text-gray-300 px-5'>
              About us
            </li>
            <li className='font-bold text-white text-3xl cursor-pointer hover:text-gray-300 px-5'>
              Our Progress
            </li>
            <li className='font-bold text-white text-3xl cursor-pointer hover:text-gray-300 px-5'>
              Careers
            </li>
            <li className='font-bold text-white text-3xl cursor-pointer hover:text-gray-300 px-5'>
              Blog
            </li>
            <li className='font-bold text-white text-3xl cursor-pointer hover:text-gray-300 px-5'>
              Start a project
            </li>
          </ul>
          <p className='text-white px-5'>hello@fundamental.bg</p>
          <div className='flex flex-row  items-center w-full space-x-3 px-5'>
            <AiOutlineFacebook size='30px' color='white' />
            <BsInstagram size='25px' color='white' />
            <AiFillLinkedin size='30px' color='white' />
          </div>
        </div>
      )}
      <div className='less:hidden md:flex flex-col justify-left md:flex-row pr-40 space-x-10'>
        <ul className='flex flex-col md:flex-row md:justify-center space-y-1 md:items-center md:space-x-10 '>
          <li className='font-bold text-white cursor-pointer hover:text-gray-300'>
            Portfolio
          </li>
          <li className='font-bold text-white cursor-pointer hover:text-gray-300'>
            About us
          </li>
          <li className='font-bold text-white cursor-pointer hover:text-gray-300'>
            Our Progress
          </li>
          <li className='font-bold text-white cursor-pointer hover:text-gray-300'>
            Careers
          </li>
          <li className='font-bold text-white cursor-pointer hover:text-gray-300'>
            Blog
          </li>
        </ul>
        <button class='bg-emerald-400 hover:bg-emerald-600 text-white font-bold py-2 px-4 rounded inline-flex justify-center items-center  w-[170px] '>
          <span>Start a project</span>
        </button>
      </div>
    </div>
  );
};

export default Header;
