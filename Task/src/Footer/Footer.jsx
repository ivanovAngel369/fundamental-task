import React from 'react';
import Logo from '../../public/picktures/logo2.png';
import { AiOutlineFacebook } from 'react-icons/ai';
import { BsInstagram } from 'react-icons/bs';
import { AiFillLinkedin } from 'react-icons/ai';

const Footer = () => {
  return (
    <div className="flex justify-center items-center w-full h-[60vh] bg-[url('../public/picktures/Lingu.png')]">
      <div className='flex flex-col md:flex-row justify-center items-center w-5/6 h-[60%] space-x-10 md:border-b-2 border-gray-600'>
        <div className='flex flex-col justify-start items-start w-full md:w-2/6 space-y-4'>
          <img src={Logo} alt='Website logo' />
          <p className='text-3xl font-bold'>We would love to hear from you</p>
          <p className='md:font-bold'>
            Feel free to reach out if you want to collaborate with us, or simply
            have a chat.
          </p>
        </div>
        <div className='flex justify-center items-center w-full'>
          <div className='flex justify-start items-start w-1/2  pt-6'>
            <ul className='space-y-2'>
              <li className='text-bold'>Pages</li>
              <li className='text-bold'>About us</li>
              <li className='text-bold'>Portfolio</li>
              <li className='text-bold'>Careers</li>
              <li className='text-bold'>Contact us</li>
            </ul>
          </div>
          <div className='flex flex-col justify-center items-start w-1/2 md:w-2/6 md:mb-5'>
            <p className='mb-3'>Location</p>
            <p>
              Fundamental Studio Ltd. 70-72 Cherni vrah blvd. Sofia, Bulgaria
              1407
            </p>
          </div>
        </div>

        {/* <div className='flex justify-start items-start w-1/2 md:w-[10%] pt-6'>
          <ul className='space-y-2'>
            <li className='text-bold'>Pages</li>
            <li className='text-bold'>About us</li>
            <li className='text-bold'>Portfolio</li>
            <li className='text-bold'>Careers</li>
            <li className='text-bold'>Contact us</li>
          </ul>
        </div>
        <div className='flex flex-col justify-center items-start w-1/2 md:w-1/6 md:mb-5'>
          <p className='mb-3'>Location</p>
          <p>
            Fundamental Studio Ltd. 70-72 Cherni vrah blvd. Sofia, Bulgaria 1407
          </p>
        </div> */}

        <div className='flex flex-col justify-start items-start w-full md:w-1/6 mt-5'>
          <div className='flex flex-col justify-start items-start'>
            <p className='mb-3'>Say Hello :)</p>
            <p>hello@fundamental.bg</p>
            <p>+359 895 697 410</p>
            <div className='flex flex-row  items-center w-full space-x-3 mt-5'>
              <AiOutlineFacebook size='30px' color='green' />
              <BsInstagram size='25px' color='green' />
              <AiFillLinkedin size='30px' color='green' />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
