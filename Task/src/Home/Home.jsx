import React from 'react';
import './Home.css';

const Home = () => {
  return (
    <div className='flex justify-center items-center w-full md:min-h-screen h-[50vh]'>
      <div
        id='intellectPick'
        className='flex justify-center items-center w-full md:min-h-screen h-[50vh]
        '
      >
        <div className='flex flex-col justify-center items-center w-full p-3'>
          <p className='lg:text-8xl md:text-6xl text-5xl  font-bold text-white'>
            Blurring the line between
          </p>
          <p className='lg:text-8xl md:text-6xl text-5xl font-bold text-white'>
            art and technology.
          </p>
          <p className=' text-white mt-8 md:text-center'>
            Fundamental Studio is a modern digital company, offering different
            kind of services at your disposal. we are your trustworthy partner.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Home;
