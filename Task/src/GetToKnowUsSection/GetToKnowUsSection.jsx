import React from 'react';
import FounderPhoto from '../../public/picktures/DSC_6187.png';

const GetToKnowUsSection = () => {
  return (
    <div className="flex flex-col justify-center items-center w-full min-h-screen bg-[url('../public/picktures/Lingu.png')] ">
      <div className="flex flex-col md:flex-row justify-start items-start min-h-screen w-5/6 bg-[url('../../public/picktures/OutlinedFont.png')]  bg-no-repeat bg-left-bottom bottom-0 mt-6 space-y-6">
        <div className='flex flex-col justify-center items-center md:justify-start md:items-start w-full md:w-1/2 h-50 md:h-[30vh] pt-20'>
          <p className='flex justify-center items-center text-green-400 w-[250px] h-[50px] border-2 border-green-400 rounded-lg p-2 mb-4'>
            Fundamentals of creativity
          </p>
          <p className='text-4xl md:text-6xl font-bold mb-4 '>Get to know us</p>
          <p className='w-4/5 text-left'>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </p>

          <button className='flex justify-center items-center w-64 md:w-32 bg-green-400 hover:bg-green-500 text-white font-bold py-4 px-4 rounded-lg  flex-row mt-8'>
            About us
            <svg
              xmlns='http://www.w3.org/2000/svg'
              fill='none'
              viewBox='0 0 24 24'
              stroke-width='1.5'
              stroke='currentColor'
              className='w-6 h-6 pl-2 '
            >
              <path
                stroke-linecap='round'
                stroke-linejoin='round'
                d='M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25'
              />
            </svg>
          </button>
        </div>
        <div className='flex flex-col justify-start items-start w-full md:w-1/2 '>
          <img
            className='h-[40vh] md:h-[80vh]'
            src={FounderPhoto}
            alt='Company founder photo'
          />
        </div>
      </div>
    </div>
  );
};

export default GetToKnowUsSection;
