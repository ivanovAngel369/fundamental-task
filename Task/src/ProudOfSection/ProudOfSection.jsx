import React from 'react';
import ProudOfSectionFirstImg from '../../public/picktures/laptop and phone.png';
import ProudOfSectionSecondImg from '../../public/picktures/IntellectTwo.png';
import ProudOfSectionThirdImg from '../../public/picktures/phonemockups.png';
import ProudOfSectionFourthImg from '../../public/picktures/laptopIntellect.png';
import './ProudOfSection.css';
import {
  ProudOfSectionBottomCardMobile,
  ProudOfSectionCardDown,
  ProudOfSectionCardRight,
  ProudOfSectionCardUp,
  ProudOfSectionTopCardMobile,
} from './ProudOfSectionCard';
import { useNavigate } from 'react-router-dom';

const ProudOfSection = () => {
  return (
    <div className="flex flex-col justify-center items-center bg-[url('../public/picktures/Lingu.png')] w-full ">
      <div className='flex flex-row justify-center items-center w-5/6 mt-6 mb-6'>
        <p className='less:text-4xl md:text-6xl font-bold md:mr-2 mt-4 '>
          Projects we are proud of
        </p>
        <div class='md:flex-grow h-px bg-gray-400 mt-6'></div>
        <button className='bg-green-600 hover:bg-green-700 text-white font-bold py-4 px-4 rounded-lg flex flex-row ml-2 mt-5 less:hidden'>
          See project
          <svg
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            stroke-width='1.5'
            stroke='currentColor'
            className='w-6 h-6 pl-2'
          >
            <path
              stroke-linecap='round'
              stroke-linejoin='round'
              d='M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25'
            />
          </svg>
        </button>
      </div>
      <div className='flex flex-col justify-center items-center w-full h-[50vh] md:w-5/6 md:min-h-screen'>
        <div
          id='intellectPick'
          className='flex justify-center items-center border-2 rounded-[25px] w-full h-[45vh]  md:w-5/6 md:h-[90vh]'
        >
          <div
            id='cardComponent'
            className='less:hidden md:flex justify-end items-end bottom-8 right-8 w-full'
          >
            <ProudOfSectionCardUp />
          </div>
          <div
            id='cardComponent'
            className='md:hidden flex justify-center items-end w-full bottom-5'
          >
            <ProudOfSectionBottomCardMobile />
          </div>

          <div
            id='cardComponent'
            className='md:hidden flex justify-center items-end w-full top-5'
          >
            <ProudOfSectionTopCardMobile />
          </div>
        </div>
        <img
          className='w-full h-[45vh] md:h-[90vh] '
          src={ProudOfSectionFirstImg}
          alt='Intellect img'
        />
      </div>

      <div className='flex justify-center items-center w-full md:w-5/6 min-h-screen mr-10 '>
        <div className='flex flex-col md:flex-col lg:flex-row w-full h-[60%] md:space-x-32'>
          {/* left */}
          <div className='flex justify-center items-center w-full h-[50vh] md:w-4/6 md:h-[70%] ml-6'>
            <div
              id='intellectPick'
              className='flex justify-center items-center border-2 rounded-[25px] w-full h-[45vh] md:w-3/6 md:h-[80%] '
            >
              <div
                id='cardComponent'
                className='less:hidden md:flex justify-start items-start bottom-8 left-8 w-full'
              >
                <ProudOfSectionCardRight />
              </div>
              <div
                id='cardComponent'
                className='md:hidden flex justify-center items-end w-full bottom-5'
              >
                <ProudOfSectionBottomCardMobile />
              </div>

              <div
                id='cardComponent'
                className='md:hidden flex justify-center items-end w-full top-5'
              >
                <ProudOfSectionTopCardMobile />
              </div>
            </div>

            <img
              className='w-full h-[45vh] md:h-[80vh] md:w-full rounded-[25px]'
              src={ProudOfSectionFourthImg}
              alt='Intellect img'
            />
          </div>
          {/* right */}
          <div className='flex justify-center items-center w-full ml-5 md:ml-0 md:w-2/6'>
            <div
              id='intellectPick'
              className='flex justify-center items-center border-2 rounded-[25px] w-full h-[45vh] md:w-[24.5%] md:h-[80%] '
            >
              <div className='less:hidden md:flex justify-end items-end w-full h-full'>
                <p className='text-white text-2xl pr-4 pb-4'>Coming soon...</p>
              </div>

              <div
                id='cardComponent'
                className='md:hidden flex justify-center items-end w-full bottom-5'
              >
                <ProudOfSectionBottomCardMobile />
              </div>

              <div
                id='cardComponent'
                className='md:hidden flex justify-center items-end w-full top-5'
              >
                <ProudOfSectionTopCardMobile />
              </div>
            </div>
            <img
              className='md:h-full w-full h-[45vh] border-2 rounded-[20px]'
              src={ProudOfSectionThirdImg}
              alt='Intellect img'
            />
          </div>
        </div>
      </div>

      <div className='flex flex-col justify-center items-center w-full h-[50vh] md:w-5/6 md:min-h-screen'>
        <div
          id='intellectPick'
          className='flex justify-center items-center border-2 rounded-[25px] w-full h-[45vh] md:w-5/6 md:h-[90vh]'
        >
          <div
            id='cardComponent'
            className='less:hidden md:flex justify-end items-end bottom-8 right-8 w-full'
          >
            <ProudOfSectionCardDown />
          </div>
          <div
            id='cardComponent'
            className='md:hidden flex justify-center items-end w-full bottom-5'
          >
            <ProudOfSectionBottomCardMobile />
          </div>

          <div
            id='cardComponent'
            className='md:hidden flex justify-center items-end w-full top-5'
          >
            <ProudOfSectionTopCardMobile />
          </div>
        </div>
        <img
          className='w-full h-[45vh] md:w-full md:h-[89.8vh] rounded-[25px] pr-1'
          src={ProudOfSectionSecondImg}
          alt='Intellect img'
        />
      </div>
    </div>
  );
};

export default ProudOfSection;
