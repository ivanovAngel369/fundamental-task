import React from 'react';
import LaptopPhoto from '../../public/picktures/laptop and phone.png';
import { CaseStudyCard } from '../ProudOfSection/ProudOfSectionCard';
import One from '../../public/picktures/a.png';
import Two from '../../public/picktures/b.png';
import Three from '../../public/picktures/c.png';
import Four from '../../public/picktures/d.png';
import ShowCase from '../../public/picktures/showcase2.png';
import PhotoMid from '../../public/picktures/HomepageMid.png';
import PhotoRight from '../../public/picktures/languageRight.png';
import PhotoLeft from '../../public/picktures/languageleft.png';

import './CaseStudyPage.css';

const CaseStudyPage = () => {
  return (
    <div className="flex flex-col justify-center items-center bg-[url('../public/picktures/Lingu.png')] w-full min-h-screen pt-10">
      <div className='flex flex-col md:flex-row justify-center items-center md:w-5/6 '>
        <div className='flex flex-col justify-center items-center md:justify-start md:items-start w-full md:w-2/5 px-10 space-y-5'>
          <CaseStudyCard />
          <p className='text-4xl md:text-6xl text-black font-bold'>
            Prevodite.net
          </p>
          <p className='text-1xl'>
            A company designed to translate and legalize important documents.
          </p>
          <p className='text-gray-600'>2022 | Client</p>
        </div>
        <div className='md:w-3/5'>
          <img src={LaptopPhoto} alt='laptop and phone photo' />
        </div>
      </div>
      <div className="flex flex-col justify-center items-center bg-[url('../public/picktures/Lingu.png')] w-full  ">
        <div className='flex flex-col justify-center items-center '>
          <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
            <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
              <img
                className='h-[15vh] md:h-[25vh]'
                src={One}
                alt='number One'
              />
              <p className='uppercase text-2xl md:text-3xl font-bold text-rose-900 mt-4 w-full px-5'>
                Obstacles
              </p>
            </div>
            <div className='flex justify-center items-center w-full md:w-1/2 '>
              <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
                The area is highly not digitalised and we strive to make this
                one a very simple and easy way to ask for a price of a
                translation online. We want to limit phone calls and have them
                make an online statement.We want to limit phone calls and have
                them make an online statement.
              </p>
            </div>
          </div>

          <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
            <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
              <img
                className='h-[15vh] md:h-[25vh]'
                src={Two}
                alt='number Two'
              />
              <p className='uppercase text-2xl md:text-3xl font-bold text-rose-900 mt-4 w-full px-5'>
                OUR MISSION
              </p>
            </div>
            <div className='flex justify-center items-center w-full md:w-1/2 '>
              <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
                The only company in Bulgaria that wants to make this kind of
                business an online business. With an easy process to making a
                request.the only company in Bulgaria that wants to make this
                kind of business an online business. With an easy process to
                making a request.
              </p>
            </div>
          </div>
          <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
            <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
              <img
                className='h-[15vh] md:h-[25vh]'
                src={Three}
                alt='number Three'
              />
              <p className='uppercase text-2xl md:text-3xl font-bold text-rose-900 mt-4 w-full px-5'>
                OUR TASK
              </p>
            </div>
            <div className='flex justify-center items-center w-full md:w-1/2 '>
              <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
                We want to limit phone calls and have them make an online
                request and order an translation. The goal is to make it more
                mobile friendly as the clients are 70% accessing the website
                from a mobile. They will get an email reminder if they forget to
                finish their payment and purchase. An email where your steps and
                your process is saved and you can access it easily.
              </p>
            </div>
          </div>
          <div className='flex flex-col md:flex-row justify-center items-center w-5/6 border-b-2 border-gray-400 md:space-x-44 pb-10 md:pb-0'>
            <div className='flex flex-row justify-start items-center w-full md:w-1/2'>
              <img
                className='h-[15vh] md:h-[25vh]'
                src={Four}
                alt='number Four'
              />
              <p className='uppercase text-2xl md:text-3xl font-bold text-rose-900 mt-4 w-full px-5'>
                Company objectives
              </p>
            </div>
            <div className='flex justify-center items-center full md:w-1/2 '>
              <p className='italic sm:not-italic md:italic lg:not-italic xl:italic ... text-gray-600 '>
                More professional. these kind of documents and services are very
                important and somewhat pricy so we need to make people trust us
                with their personal information.more professional. these kind of
                documents and services are very important and somewhat pricy so
                we need to make people trust us with their personal information.
              </p>
            </div>
          </div>
        </div>
        <div className='flex flex-col justify-center items-center w-full h-[40vh] md:min-h-screen mb-9 overflow-x-hidden overflow-y-hidden '>
          <img
            className='scale-x-150s md:scale-125 translate-x-4 skew-y-3 '
            src={ShowCase}
            alt='Images'
          />
        </div>
        <div className='flex flex-col justify-center items-center w-4/5 space-y-10'>
          <div className='flex flex-row justify-center items-center w-5/6  space-x-10'>
            <p className='text-3xl md:text-5xl mr-2 mt-4'>Color plate</p>
            <div class='flex-grow h-px bg-gray-400 mt-6'></div>
          </div>
          <div className='flex flex-col justify-center items-center md:flex-row space-y-5 md:space-x-5'>
            <div className='md:w-1/5'>
              <div id='block1' className='w-[180px] h-[180px] rounded-lg'></div>
            </div>
            <div className='md:w-1/5'>
              <div id='block2' className='w-[180px] h-[180px] rounded-lg'></div>
            </div>
            <div className='md:w-1/5'>
              <div id='block3' className='w-[180px] h-[180px] rounded-lg'></div>
            </div>
            <div className='md:w-1/5'>
              <div id='block4' className='w-[180px] h-[180px] rounded-lg'></div>
            </div>
            <div className='md:w-1/5'>
              <div id='block5' className='w-[180px] h-[180px] rounded-lg'></div>
            </div>
          </div>
          <div className='flex flex-row justify-center items-center w-5/6  space-x-10 pb-5'>
            <p className='text-3xl md:text-5xl mr-2 mt-4 '>Typography</p>
            <div class='flex-grow h-px bg-gray-400 mt-6'></div>
          </div>
        </div>
      </div>
      <div className="flex flex-col justify-center items-center bg-[url('../public/picktures/Lingu.png')] w-full min-h-screen space-y-10">
        <div className='flex flex-row justify-center items-center space-x-10'>
          <div className='flex w-2/6 '>
            <p className='text-bold text-2xl '>
              ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 0123456789
              -«+»!?.*\\/()£€$¥¢+−±×÷=≠≈≤
            </p>
          </div>
          <div className='w-2/6'>
            <p className='text-bold text-2xl'>The sans font</p>
            <p>
              feather bold is the typeface we use for headlines, when we want to
              shout out and get noticed. it's bespoke to duolingo, so no one
              else can use it. but let's make sure that we use it nicely!
            </p>
          </div>
        </div>
        <div className='flex flex-row justify-center items-center space-x-10'>
          <div className='flex flex-col justify-center items-center space-y-5 w-2/6'>
            <div className='flex flex-col justify-start items-start w-full'>
              <p className='w-full text-3xl'>Change the world.</p>
              <p className=' text-1xl'>One word at a time.</p>
            </div>
            <div>
              <p className='text-bold text-2xl'>300 million downloads.</p>
              <p>
                We launched a revolution in language learning and we're just
                getting started.
              </p>
            </div>
            <div>
              <p className='text-bold text-2xl'>Linguist:</p>
              <p>
                a person who changes the world by making language learning fast
                and fun.
              </p>
            </div>
          </div>
          {/*  */}
          <div className='flex flex-col justify-center items-center space-y-5 w-2/6 pb-4'>
            <div className='flex flex-col justify-start items-start w-full'>
              <p className='w-full text-3xl'>Combining Typefaces</p>
              <p className=' text-1xl'>
                Bring the two fonts together by using equal leading throughout
                the block of copy when possible.
              </p>
            </div>
            <div className='flex flex-col justify-start items-start w-full'>
              <p className='font-bold'>Heading - The Sans Bold</p>
              <p className='font-bold'>Body text - Raleway Regular</p>
            </div>
            <div>
              <p>
                Always use them by the rules of Hierarchy. To achieve a
                harmonious relationship between our logotype and headlines, make
                the body text significantly smaller but readable to ensure
                there's a contrast in weight and scale.
              </p>
            </div>
          </div>
        </div>
      </div>

      <div className="flex flex-row justify-center items-center bg-[url('../../public/picktures/showcaase 2.png')] w-full min-h-screen pt-10">
        <div className='flex justify-center items-center '>
          <img
            className='absolute h-[80vh] z-20'
            src={PhotoMid}
            alt='showcase'
          />
          <img
            id='left'
            className='absolute h-[80vh] z-10 right-36'
            src={PhotoLeft}
            alt='showcase'
          />
          <img
            id='right'
            className='absolute h-[80vh] z-10 left-24'
            src={PhotoRight}
            alt='showcase'
          />
        </div>
      </div>
    </div>
  );
};

export default CaseStudyPage;
