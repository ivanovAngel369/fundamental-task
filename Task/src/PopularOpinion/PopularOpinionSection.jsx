import React from 'react';
import StarGroup from '../../public/picktures/starGroup.png';
import Customers from '../../public/picktures/Customers.png';
import Images from '../../public/picktures/showcase.png';

const PopularOpinionSection = () => {
  return (
    <div className="flex flex-col justify-center items-center bg-[url('../public/picktures/Lingu.png')] w-full min-h-screen ">
      <div className='flex flex-col justify-center items-center w-full'>
        <div className='flex flex-row justify-center items-center w-5/6 mb-10'>
          <p className='text-3xl md:text-5xl font-bold mr-2 mt-4'>
            See what people think about us
          </p>
          <div class='flex-grow h-px bg-gray-400 mt-6'></div>
        </div>
        <div className='flex flex-col md:flex-row justify-center items-center w-5/6 mt-6 md:space-x-5 space-y-3 md:space-y-0'>
          <div
            className="flex flex-col justify-center items-center bg-[url('../public/picktures/gm1.png')] bg-cover w-full h-[50vh] 
          md:w-1/3 md:h-[64vh] border-2 rounded-[25px] pb-5"
          >
            <p className='flex justify-center items-center text-6xl text-white w-full'>
              Clutch
            </p>
            <img className='h-5 w-25' src={StarGroup} alt='Five star group' />
            <div className='w-[40%] h-[7vh] bg-white rounded-[10px] mt-10'>
              <p className='flex justify-center items-center w-full h-full text-2xl'>
                Rating 5.0
              </p>
            </div>
          </div>
          <div className='flex flex-col justify-left items-start bg-zinc-300 w-full h-[50vh] md:w-1/3 md:h-[64vh] border-2 rounded-[25px]'>
            <img className='p-5' src={StarGroup} alt='Five star group' />
            <div className='flex flex-col justify-center items-center w-full h-full'>
              <p className='p-5 md:p-8 text-left md:pb-10'>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
              <div className='flex flex-col justify-start items-start w-full p-8 pb-4 pt-10'>
                <p className='w-full font-bold text-2xl'>Martin Stefanov</p>
                <p className='w-full'>CEO of a company</p>
              </div>
            </div>
          </div>
          <div className='flex flex-col justify-left items-start bg-zinc-300 w-full h-[50vh] md:w-1/3 md:h-[64vh] border-2 rounded-[25px]'>
            <img className='p-5' src={StarGroup} alt='Five star group' />
            <div className='flex flex-col justify-center items-center w-full h-full'>
              <p className=' p-5 md:p-8 text-left md:pb-10'>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
              <div className='flex flex-col justify-start items-start w-full p-8 pb-4 pt-10'>
                <p className='w-full font-bold text-2xl'>Martin Stefanov</p>
                <p className='w-full'>CEO of a company</p>
              </div>
            </div>
          </div>
        </div>
        <div>
          <img src={Customers} alt='Customers' />
        </div>
      </div>
      <div className='flex flex-col justify-center items-center w-full h-[40vh] md:min-h-screen mb-9 overflow-x-hidden overflow-y-hidden '>
        <img
          className='scale-x-150s md:scale-125 translate-x-4 skew-y-3 '
          src={Images}
          alt='Images'
        />
      </div>
    </div>
  );
};

export default PopularOpinionSection;
